# Secure TICK Stack in Containerized Environment

Contains docker-compose configuration and other default configurations for Telegraf, InfluxDB, Chronograf and Kapacitor instances.
The containers are secured using following methods,

- Telegraf, InfluxDB and Kapacitor containers do not forward ports to the host. Their ports are inaccessible from outside. 
Only the port 8888 of Chronograf instance is forwarded to host to make sure we can access the frontend.
- All the containers use a secure bridge 'safe-net' to communicate. 
This isolates the network traffic from other containers on same host.
- The cloud instance running the containers along with Chronograf only exposes port 8888 for http access.
- The fronted has extra layer of security with HTTP basic auth.

